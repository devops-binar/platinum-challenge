resource "google_compute_instance" "gitlab-runner" {
    name = "terraform-gitlab-runner"
    machine_type = "e2-micro"
    boot_disk {
      initialize_params {
        image = "ubuntu-os-cloud/ubuntu-2204-lts"
      }
    }
    metadata = {
        ssh-keys = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCFQbHPlUCCR57H+iQk5r+gWx7MdDKNGhV30AjAlBiiCbgvhdkxaZ7Lo4FBC0yWslDl7EBBl9Css8G4E8VyBCSYAvrzWrwmoB36UHrllhAHHlVSD83NuLXTV8T2Fn6FvqlMuEs4ogE7UEr3X9qY89tN4pJJw8VD1DsnzfHDXysTGTy9YOB8f5R8U8QCp/Jq7NsM4JcAxDm6/OGxic03DWXXzmbnwaCTBrGDmeIa8qhJul7XIU9Z9JKc8Jquc4O/6P3VS10284/2pNtW9ITfeuCvS5eyz8L3YRykmotDtB5ERIjbgkoaMsHPoeQpdMbFB1fu1vJbpQVCFxp9cZg0gHeG6esAbi18xWAG7IyrdhrrEfzrwzz9n7rkANqeoPs+ngrVHy9A2v8wMz6qXMc6ijXkLukYg7ACQhIegbDBBSIqPhFKHn2H+mBi3vhX+/QBwsf9s/G9/CEE4p3xkrrcWrx+Xfe0CuUNNtjKCYII/OCMTozpcT3z0fBmjuOBAKRrcl0= randyansyah@DESKTOP-NJRKC6U"
        startup_script = <<SCRIPT
            #! /bin/bash
            sudo ufw allow 22        
            SCRIPT
    }
    tags = ["http-server"]
    network_interface {
      network = "default"
      access_config {

      }
    }
}