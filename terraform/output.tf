output "output_user_password" {
    description = "Password dari User"
    value = random_id.binar_sql_user_password.id
}

output "output_sql_private_ip" {
    description = "Private IP dari SQL instance"
    value = google_sql_database_instance.binar_sql_instance.private_ip_address
}

output "output_sql_public_ip" {
    description = "Public IP dari SQL instance"
    value = google_sql_database_instance.binar_sql_instance.public_ip_address
}