resource "random_id" "binar_sql_user_password" {
    byte_length = 8
}

resource "google_sql_user" "binar_sql_user" {
    name = "people"
    password = random_id.binar_sql_user_password.id
    instance = google_sql_database_instance.binar_sql_instance.name
}

resource "google_sql_database" "binar_sql_db" {
    name = "people"
    instance = google_sql_database_instance.binar_sql_instance.name
}