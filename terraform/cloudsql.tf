resource "google_sql_database_instance" "binar_sql_instance" {
    name = "binar-sql"
    database_version = "POSTGRES_14"
    deletion_protection = false
    settings {
      tier = "db-custom-1-3840"
      availability_type = "ZONAL"
      disk_size = 10
      activation_policy = "ALWAYS"
      deletion_protection_enabled = false
      ip_configuration {
        ipv4_enabled = true
        private_network = "projects/rare-palace-374712/global/networks/default"

        authorized_networks {
          name = "internet"
          value = "0.0.0.0/0"
        }
      }
    }
}


