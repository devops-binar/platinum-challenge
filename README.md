# Fullstack JS (reactjs, expressjs, postgresql)

Simple CRUD app built with ReactJS, ExpressJS, PostgreSQL.

link platinum challenge repositories : [https://gitlab.com/devops-binar/platinum-challenge](https://gitlab.com/devops-binar/platinum-challenge)

dev app link : [dev.belajardevops.site](https://dev.belajardevops.site/)

prod app link : [prod.belajardevops.site](https://prod.belajardevops.site/)

![latinum_challenge_topologi](/uploads/412580dbf0c552b0135d7f302637f888/latinum_challenge_topologi.png)

## requirement
- install gcloud cli di local. [guide](https://cloud.google.com/sdk/docs/install#linux)
- install kubectl di local. [guide](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
- instal terraform di local. [guide](https://developer.hashicorp.com/terraform/tutorials/gcp-get-started/install-cli)
- install ansible di local. pastikan ansible version yang di instal minimal versi 2.12 [guide](https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html#installing-ansible-on-ubuntu)

## create postgresql using terraform

1. untuk melakukan create cloudsql postgres kita harus masuk terlebih dahulu ke folder terraform lalu gunakan command terraform di bawah ini
```
cd ~/platinum-challenge/terrafotm
terraform init 
terraform plan 
terraform apply
```
pastikan cloudsql postgres berhasil di buat
![cloudsql](/uploads/a1dc1c5914a3f3eb4561950557eb30da/cloudsql.png)

2. save info user database dan password yaang ada pada file terraform.tfstate yg ada di dalam folder terraform

3. buat tabel serta input data pada database yg telah di buat dengan menggunakan command di bawah ini
```
psql -p 5432 -h 34.xxx.xxx.xxx -U people -W people
```

```sql
-- for postgresql
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  name varchar(30),
  email varchar(30),
  gender varchar(6)
);

INSERT INTO users (name, email, gender) VALUES
('John Doe', 'john@gmail.com', 'Male'),
('Mark Lee', 'mlee@gmail.com', 'Male'),
('Sofia', 'sofia@gmail.com', 'Female'),
('Michelle', 'mangela@gmail.com', 'Female');

exit
```

## create GKE cluster

buat GKE kubernetes cluster menggunakan google cloud console [guide](https://cloud.google.com/kubernetes-engine/docs/how-to/creating-a-zonal-cluster)

pastikan cluster telah dibuat

![gke](/uploads/dc9d4d56af299f00d8cf1c74c61c1c1e/gke.png)

lalu connectkan cluster yg telah dibuat ke local computer dengan command ini
```
gcloud container clusters get-credentials platinumcluster --zone asia-southeast2-a --project rare-palace-374712
```
## create backend&frontend docker image

Dockerfile backend
```
FROM node:18.9.1-alpine3.16

WORKDIR /app

COPY backend/. .

RUN npm install

CMD ["node", "server.js" ]
```

Dockerfile frontend
```
FROM node:18.9.1-alpine3.16

WORKDIR /app

COPY frontend/. .

RUN npm install

EXPOSE "8081"

CMD [ "npm", "start" ]
```
kemudian push image ke dockerhub, pastikan image telah di push di dockerhub repositories

dockerhub repositories link : [bluestack007](https://hub.docker.com/repositories/bluestack007)

# apply kubernetes manifest

## create namespaces staging & production

masuk ke folder kubernetes lalu apply file ns.yaml menggunakan kubectl
```
cd ~/platinum-challenge/kubernetes
kubectl apply -f ns.yaml
```
## create staging deployment

masuk ke folder staging lalu apply file ssl.yaml dan ingress.yaml dengan menngunakan namespace staging
```
cd ~/platinum-challenge/kubernetes/staging
kubectl apply -f ssl.yaml -n staging
kubectl apply -f ingress.yaml -n staging
```
kemudian apply semua file yg terdapat di dalam folder backend 
```
cd ~/platinum-challenge/kubernetes/staging/backend
kubectl apply -f secret.yaml -n staging
kubectl apply -f configmap.yaml -n staging
kubectl apply -f deployment.yaml -n staging

``` 
kemudian apply juga semua file yang ada di dalam folder frontend
```
cd ~/platinum-challenge/kubernetes/staging/frontend
kubectl apply -f configmap.yaml -n staging
kubectl apply -f deployment.yaml -n staging

``` 
kemudian dapatkan ip addres dari ingress load balancer emggunakan namespace staging
```
kubectl get ingress -n staging`
```
kemudian daftar ip addres ingress tersebut ke dns server domain di sini saya menggunakan dns cloudflare

![dns_cloudflare](/uploads/c574180d348f979dcfd1fdac0e6212bc/dns_cloudflare.png)

kemudian akses link  [https://dev.belajardevops.site/](https://dev.belajardevops.site/) apakah berhasil di deploy
![dev_ssl](/uploads/b78bac80542bec7bfe65ae019cc6788d/dev_ssl.png)

## create production deploy

untuk deploy production lakukan juga hal sama pada folder production bedanya hanya mengganti namespace staging menjadi namespace production

# create CI CD untuk kubernetes cluster

## Buat GCP service account

Lihat project ID yang digunakan :

```
gcloud projects list
```

Set project ID yang akan digunakan :

```
export PROJECT_ID=<ISI-DENGAN-PROJECT-ID>
```

Buat sebuah service account yang akan digunakan oleh Gitlab Runner untuk mengambil credential Kubernetes Clusternya :

```
gcloud iam service-accounts create k8s-runner --display-name k8s-runner --description "k8s runner"
gcloud projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:k8s-runner@$PROJECT_ID.iam.gserviceaccount.com" --role="roles/logging.logWriter"
gcloud projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:k8s-runner@$PROJECT_ID.iam.gserviceaccount.com" --role="roles/monitoring.metricWriter"
gcloud projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:k8s-runner@$PROJECT_ID.iam.gserviceaccount.com" --role="roles/container.developer"
```

Buat sebuah key yang akan dipasang di Gitlab sebagai sebuah variable :

```
gcloud iam service-accounts keys create ./k8s-runner.json --iam-account=k8s-runner@$PROJECT_ID.iam.gserviceaccount.com
```

Lakukan encode sederhana :

```
base64 k8s-runner.json
```

## Membuat variable di Gitlab

### Variable untuk GCP

Buat sebuah variable di project Gitlab. Copy output dari command di atas dan paste di bagian `Value`. Beri nama variablenya `SERVICE_ACCOUNT_KEY` :

![service_account_1](/uploads/297ad85cd9eba2a88d8b8f1c11e82b21/service_account_1.png)
![service_account_2](/uploads/ca35c3c5fe2c8193a40df583dfca3fe9/service_account_2.png)

Buat sebuah variable dengan nama `GKE_CLUSTER_NAME` dan `GKE_CLUSTER_ZONE` yang berisi nama dari Kubernetes clusternya dan Zone-nya.

### Variable untuk Dockerhub

Buat variable untuk login ke Dockerhub dengan nama `CI_REGISTRY_PASSWORD` dan `CI_REGISTRY_USERNAME`. Isi dengan username dan password dari Dockerhub.

![gitlab_variable](/uploads/5b7614124fd065358e6c474e5717a19b/gitlab_variable.png)

## create gitlab runner using helm chart

masuk ke folder helm-runner kemudian jalankan kode di bawah ini
```
cd ~/platinum-challenge/helm-runner
# pertama create gitlab namespace
kubectl apply -f namespace.yaml
# kemudian create gitlab secret yg berisi gitlab runner token
kubectl apply -f gitlab-secret.yaml -n gitlab
# next deploy gitlab runner
kubectl apply -f values.yaml -n gitlab
```

## create gitlab runner using ansible

masuk ke folder ansible kemudian jalankan playbook.yaml menggunakan command berikut
```
cd ~/platinum-challenge/ansible
ansible-playbook playbook.yml -i inventory.ini
```
pastikan tidak ada task yang failed
![ansible_2](/uploads/d62c78c0653c4ce7b92c9f281a5e09a8/ansible_2.png)

apabila proses instalasi gitlab runner berhasil maka akan terdapat 2 buat gitlab runner pada CI CD satu di instal menggunakan helm chart yaitu gitlab runner yg menggunakan executor kubernets dan satu lagi di instal menggunakan ansible yaitu gitlab runner yg menggunakan executor docker
![ansible_1](/uploads/3d7fcd23c2a8798720226ee8c289c7d3/ansible_1.png)

## run CI CD pipeline

untuk runing ci cd pipepline kita melakukan triger pada ci cd pipeline yaitu kita harus melakukan update code di local komputer kemudian commit & push ke gitlab repositories apabila telah di push maka secara otomatis pipeline akan running 
![pipeline_1](/uploads/a09203ccc8ac8614cff886bfff8fbe30/pipeline_1.png)
![pipeline_2](/uploads/73a9824fa821ec0e67cd428d71a5a838/pipeline_2.png)
![pipeline_3](/uploads/5a43929d5e1a91a8b69005354ad12de6/pipeline_3.png)
![pipeline_4](/uploads/fae25142ee5e6acfd0c6a91cbaa8aae4/pipeline_4.png)

## monitoring & logging

apabila dev/staging dan production telah berjalan maka langkah selanjutnya adalah melakukan monitoring app dan infra disini kita akan mengunakan google cloud monitoring 
![monitoring](/uploads/0f096736891953a0eac2e195b663ac98/monitoring.png)

dan untuk keperluan audit dan troubleshooting kita menggunakan google cloud logging
![logging](/uploads/9f81e04b7664a140809e3b9e7cb58a2d/logging.png)



# Credit

All credit goes to [M. Fikri](https://www.youtube.com/watch?v=es9_6RFR7wk&t=3336s) as creator of this app.

App used:

[Frontend](https://github.com/mfikricom/Frontend-React-MySQL)
[Backend](https://github.com/mfikricom/Backend-API-Express-MySQL)
